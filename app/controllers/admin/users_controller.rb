class Admin::UsersController < ApplicationController
  let :ADMIN, :all
  skip_before_action :verify_authenticity_token

  def index
  end

  def get_users
    render json: { users: User.all.as_json(include: :type_user) }
  end
end
