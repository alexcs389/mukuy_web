class Admin::ReportsController < ApplicationController
  let :ADMIN, :all
  before_action :set_report, only: [:show, :update]
  skip_before_action :verify_authenticity_token

  def index
  end

  def show
    render json: {report: @report.as_json(include: :user)}
  end

  def update
    @report.status_report_id = params[:status_report_id]
    @report.priority_id = params[:priority_id]
    if @report.save
      render json: {success: true}, status: :ok
    else
      render json: {errors: @report.errors.full_messages}, status: :unprocessable_entity
    end
  end

  def get_reports
    render json: { reports: Report.all.as_json }
  end

  private

  def set_report
    @report = Report.find(params[:id])
  end
end
