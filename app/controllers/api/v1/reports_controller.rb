class Api::V1::ReportsController < ApplicationController
  let :all, :all
  before_action :set_report, only: [:show, :update]
  skip_before_action :verify_authenticity_token

  def show
    render json: {report: @report.as_json(except: [:user_id, :city_id, :priority_id, :status_report_id, :updated_at])}
  end

  def create
    @report = Report.new(report_params)
    @report.priority_id = Priority::UNASSIGNED
    @report.status_report_id = StatusReport::PENDING

    if @report.save
      render json: {success: true}, status: :created
    else
      render json: {errors: @report.errors.full_messages}, status: :unprocessable_entity
    end
  end

  def update
    if @report.update(report_params)
      render json: {success: true}, status: :ok
    else
      render json: {errors: @report.errors.full_messages}, status: :unprocessable_entity
    end
  end

  private

  def set_report
    @report = Report.find(params[:id])
  end

  def report_params
    params.permit(:title, :description, :latitude, :longitude, :image, :user_id)
  end
end
