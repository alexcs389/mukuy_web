class Api::V1::UsersController < ApplicationController
  let :all, :all
  before_action :set_user, only: [:show]
  skip_before_action :verify_authenticity_token

  def show
    render json: {user: @user.as_json(except: [:password, :password_salt, :type_user_id, :updated_at])}
  end

  def create
    user = User.new(user_params)
    user.type_user = TypeUser.find(TypeUser::CLIENT)

    if user.save
      render json: {
        success: true, user: user.as_json(
          except: [:id, :password, :password_salt, :created_at, :updated_at, :type_user_id]
        )
      }, status: :created
    else
      render json: {errors: user.errors.full_messages},
             status: :unprocessable_entity
    end
  end

  def login
    user = User.authenticate(params[:email], params[:password])
    if user && user.type_user.id == TypeUser::CLIENT
      render json: {success: true, user: user.as_json(except: [:password, :password_salt])}, status: :ok
    else
      render json: {success: false, error: 'El usuario no existe o las credenciales son invalidas.'}, status: :ok
    end
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:username, :password, :name, :lastname,
                                 :email, :dependence, :job, :phone_number)
  end
end
