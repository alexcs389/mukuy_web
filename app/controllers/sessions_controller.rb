class SessionsController < ApplicationController
  let :all, :all
  include SessionsHelper

  def new
    if logged_in?
      enter
    else
      render :action => 'new', :layout => false
    end
  end

  def create
    user = User.authenticate(params[:email], params[:password])
    if user && user.type_user.id == TypeUser::ADMIN
      log_in user
      enter
    else
      flash[:notice] = 'Correo electrónico o contraseña incorrectos'
      render :action => 'new', :layout => false
    end
  end

  def destroy
    log_out if logged_in?
    flash[:notice] = nil
    redirect_to login_path
  end
end
