class ApplicationController < ActionController::Base
  lock_access
  let :all
  layout :get_layout
  include SessionsHelper

  def unauthorized_access_redirection_path
    # Ensure an array of symbols
    if current_user.nil?
      login_path
    else
      clearance_levels = Array(current_user.clearance_levels).map(&:to_sym)
      return e401_path if clearance_levels.include?(:ADMIN)
    end
  end

  def unauthorized_view_render
    render 'errors/401', layout: false
  end

  private

  def get_layout
    my_class_name = self.class.name
    if my_class_name.index("::").nil?
      layout = "application"
    else
      layout = my_class_name.split("::").first.downcase
    end
    layout
  end
end
