class User < ApplicationRecord
  belongs_to :type_user

  validates :email, presence: true, uniqueness: true
  validates :username, uniqueness: true, if: -> { username.present? }
  validates :password, presence: true, :on => :create
  validates :password, presence: true, if: :change_password

  before_create :password_hash
  before_update :password_hash, if: :change_password
  attr_accessor :change_password

  def clearance_levels
    self.type_user.name
  end

  def password_hash
    var = if password.present?
            self.password_salt = BCrypt::Engine.generate_salt
            self.password = BCrypt::Engine.hash_secret(password, password_salt)
          end
    var
  end

  def self.authenticate(email, password)
    user = find_by_email(email)
    if user && user.password == BCrypt::Engine.hash_secret(password, user.password_salt)
      user
    else
      nil
    end
  end
end
