class Report < ApplicationRecord
  belongs_to :user
  belongs_to :priority
  belongs_to :status_report

  mount_uploader :image, ImageUploader

  validates :title, :description, :latitude, :longitude, :user, presence: true
end
