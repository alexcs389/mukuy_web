class Priority < ApplicationRecord
  has_many :reports

  UNASSIGNED = 1
  LOW = 2
  HALF = 3
  HIGH = 4
end
