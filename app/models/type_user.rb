class TypeUser < ApplicationRecord
  has_many :users

  ADMIN = 1
  CLIENT = 2
end
