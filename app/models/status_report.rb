class StatusReport < ApplicationRecord
  has_many :reports

  PENDING = 1
  IN_PROCESS = 2
  ATTENDED = 3
end
