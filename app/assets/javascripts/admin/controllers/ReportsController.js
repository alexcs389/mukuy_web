app.controller('ReportsController', ['$scope', '$http', function($scope, $http) {
    $scope.report = {};
    $scope.reports = [];

    angular.element(document).ready(function () {
        $scope.getReports();
    });

    $scope.getReports = function () {
        $http.get('/admin/reports/get_reports').then(function (response) {
            $scope.reports = response.data.reports;
        });
    }

    $scope.showReport = function (id) {
        $http.get('/admin/reports/'+id).then(function (response) {
            $scope.report = response.data.report;
            initMap($scope.report.latitude, $scope.report.longitude);
            $('#showReportModal').appendTo('body').modal('show');
        });
    }

    $scope.updateReport = function () {
        $http.put('/admin/reports/'+$scope.report.id, $scope.report).then(function (response) {
            if (response.data.success) {
                Swal.fire({
                    title: 'Actualizado!',
                    text: "El reporte se ha actualizado de manera correcta",
                    type: 'success'
                }).then((result) => {
                    if (result.value) {
                        $scope.getReports();
                        $('#showReportModal').modal('hide');
                    }
                });
            }
        });
    }

    function initMap(lat, lng) {
        var position = {lat: parseFloat(lat), lng: parseFloat(lng)};
        var map = new google.maps.Map(document.getElementById('map'), {
            center: position,
            zoom: 18,
            mapTypeId: 'hybrid'
        });
        new google.maps.Marker({position: position, map, title: "Hello World!"});
    }
}]);