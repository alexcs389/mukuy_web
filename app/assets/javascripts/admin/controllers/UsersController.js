app.controller('UsersController', ['$scope', '$http', function($scope, $http) {
    $scope.users = [];

    angular.element(document).ready(function () {
        $scope.getUsers();
    });

    $scope.getUsers = function () {
        $http.get('/admin/users/get_users').then(function (response) {
            $scope.users = response.data.users;
        });
    }
}]);