// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery3
//= require popper
//= require bootstrap
//= require angular
//= require rails-ujs
//= require activestorage
//= require turbolinks
//= require sweetalert2
// require jquery-3.2.1.min
// require bootstrap-4.1/popper.min.js
// require bootstrap-4.1/bootstrap.min.js
//= require slick/slick.min
//= require wow/wow.min
// require animsition/animsition.min.js
//= require bootstrap-progressbar/bootstrap-progressbar.min.js
//= require counter-up/jquery.waypoints.min.js
//= require counter-up/jquery.counterup.min.js
//= require circle-progress/circle-progress.min.js
//= require perfect-scrollbar/perfect-scrollbar.js
//= require chartjs/Chart.bundle.min.js
//= require select2/select2.min.js
//= require main