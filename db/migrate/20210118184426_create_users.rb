class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :username
      t.string :password
      t.string :name
      t.string :lastname
      t.string :email
      t.string :dependence
      t.string :job
      t.string :phone_number
      t.references :type_user

      t.timestamps
    end
  end
end
