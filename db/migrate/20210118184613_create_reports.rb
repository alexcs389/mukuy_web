class CreateReports < ActiveRecord::Migration[5.2]
  def change
    create_table :reports do |t|
      t.string :title
      t.text :description
      t.string :latitude
      t.string :longitude
      t.string :image
      t.references :user
      t.references :city
      t.references :priority

      t.timestamps
    end
  end
end
