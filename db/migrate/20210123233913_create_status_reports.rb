class CreateStatusReports < ActiveRecord::Migration[5.2]
  def change
    create_table :status_reports do |t|
      t.string :name

      t.timestamps
    end
  end
end
