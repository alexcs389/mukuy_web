class AddStatusReportToReports < ActiveRecord::Migration[5.2]
  def change
    add_reference :reports, :status_report, foreign_key: true
  end
end
