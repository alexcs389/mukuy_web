after :type_users do
  User.find_or_create_by!(
    username: 'admin',
    name: 'Alejandro',
    lastname: 'Cortés',
    dependence: '',
    job: '',
    phone_number: '',
    email: 'admin@admin.com',
    password: 'asd12345678',
    type_user: TypeUser.find(TypeUser::ADMIN)
  )
end