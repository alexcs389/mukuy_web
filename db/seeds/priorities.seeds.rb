Priority.find_or_create_by!(id: Priority::UNASSIGNED, name: 'Sin asignar')
Priority.find_or_create_by!(id: Priority::LOW, name: 'Baja')
Priority.find_or_create_by!(id: Priority::HALF, name: 'Media')
Priority.find_or_create_by!(id: Priority::HIGH, name: 'Alta')
