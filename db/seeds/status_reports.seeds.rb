StatusReport.find_or_create_by!(id: StatusReport::PENDING, name: 'Pendiente')
StatusReport.find_or_create_by!(id: StatusReport::IN_PROCESS, name: 'En proceso')
StatusReport.find_or_create_by!(id: StatusReport::ATTENDED, name: 'Atendido')