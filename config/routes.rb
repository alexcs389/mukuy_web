Rails.application.routes.draw do
  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'sessions#new'

  get 'login' => 'sessions#new'
  post 'login' => 'sessions#create'
  get 'logout' => 'sessions#destroy'
  get 'e401' => 'application#unauthorized_view_render'

  namespace :admin do
    get 'home' => 'home#index'

    resources :reports do
      collection do
        get 'get_reports' => 'reports#get_reports'
      end
    end
    resources :users do
      collection do
        get 'get_users' => 'users#get_users'
      end
    end
  end

  namespace :api, path: '/', defaults: {format: :json} do
    namespace :v1 do

      resources :users, only: [:create, :show] do
        collection do
          post 'login'
        end
      end

      resources :reports
    end
  end
end
